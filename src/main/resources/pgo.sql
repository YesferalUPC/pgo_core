-- MySQL dump 10.16  Distrib 10.1.18-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: pruebitasGO
-- ------------------------------------------------------
-- Server version	10.1.18-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `categoria_id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_nombre` varchar(50) NOT NULL,
  `categoria_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`categoria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'Golosinas',1),(2,'Bebida',1),(3,'Enlatado',1),(4,'Mascota',1),(5,'Lacteo',1);
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `producto_id` int(11) NOT NULL AUTO_INCREMENT,
  `categoria_id` int(11) NOT NULL,
  `producto_nombre` varchar(50) NOT NULL,
  `producto_descripcion` text NOT NULL,
  `producto_stock` int(11) NOT NULL,
  `producto_perecible` tinyint(1) NOT NULL,
  `producto_fVencimiento` date NOT NULL,
  `producto_precio` float NOT NULL,
  `producto_estado` tinyint(1) NOT NULL,
  PRIMARY KEY (`producto_id`),
  KEY `categoria_id` (`categoria_id`),
  CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`categoria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,1,'SUBLIME','CHOCOLATE',1,0,'2016-09-30',1,0),(2,1,'SUBLIME','CHOCOLATE',1,0,'2016-09-30',1,0),(3,1,'SUBLIME','CHOCOLATE',1,0,'2016-09-30',1,0),(4,1,'A COCA COLA','A GASEOSA',2,0,'2016-10-05',2,0),(5,1,'7UP','GASEOSA',3,0,'2016-10-06',3,0),(6,1,'INKA COLA','GASEOSA',1,0,'2016-09-29',1,0),(7,1,'7UP','GASEOSA',3,1,'2016-10-06',3,0),(8,1,'7UP','GASEOSA',3,1,'2016-10-06',3,0),(9,1,'INKA COLA','GASEOSA',1,0,'2016-09-29',1,0),(10,1,'INKA COLA','GASEOSA',1,0,'2016-09-29',1,0),(11,1,'PEPSI','GASEOSA',1,0,'2016-09-29',1,0),(12,1,'SUBLIME','CHOCOLATE',1,1,'2016-09-30',1,0),(13,1,'SUBLIME','CHOCOLATE',1,0,'2016-09-30',1,0),(14,1,'INKA COLA','GASEOSA',1,0,'2016-09-29',1,0),(15,1,'PEPSI','AGASEOSA',2,1,'2016-09-29',2,0),(16,1,'INKA COLA','GASEOSA',1,0,'2016-09-29',1,0),(17,1,'PEPSI','AGASEOSA',2,1,'2016-09-29',2,0),(18,1,'INKA COLA','GASEOSA',1,0,'2016-09-29',1,0),(19,2,'PEPSI','AGASEOSA',2,1,'2016-09-29',2,0),(20,1,'SUB','CHOCOLATE',1,1,'2016-09-30',1,0),(21,1,'NUEVO','CHOCOLATE',1,1,'2016-09-30',2.12,0),(22,1,'NUEVO','CHOCOLATE',1,0,'2016-09-30',1,0),(23,1,'INKA COLA','GASEOSA',1,0,'2016-09-29',1,0),(24,2,'PEPSI','AGASEOSA',2,1,'2016-09-29',2,0),(25,1,'NUEVO','CHOCOLATE',1,0,'2016-09-30',1,0),(26,2,'COCA COLA','GASEOSA DE 2 LITROS',1,0,'2017-01-01',4.5,1),(27,1,'SUBLIME','CHOCOLATE CLASICO',1,0,'2017-01-01',1,0),(28,1,'HALLS','BARRA DE CARAMELOS',1,0,'2017-01-01',1,1),(29,1,'CHARADA','GALLETA CLASICA',10,1,'2017-03-01',0.5,0),(30,1,'CHARADA','GALLETA CLASICA',5,0,'2017-01-01',0.5,1),(31,1,'INKA COLA','GASEOSA',1,0,'2016-10-17',1,0),(32,2,'PEPSI','AGASEOSA',2,1,'2016-10-17',2,0),(33,2,'PEPSI','GASEOSA DE 1L',8,1,'2017-03-01',3,0),(34,2,'PEPSI','GASEOSA DE 1L',12,1,'2017-02-15',3,1),(35,1,'CHARADA','PAQUETE DE GALLETA',1,1,'2017-01-04',0.5,1);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `usuario_id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario_nombre` varchar(50) NOT NULL,
  `usuario_apellido` varchar(50) NOT NULL,
  `usuario_correo` varchar(50) NOT NULL,
  `usuario_clave` varchar(25) NOT NULL,
  `usuario_rol` int(11) NOT NULL,
  PRIMARY KEY (`usuario_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'koffing','pokemon','koffing@pgo.com','12345',1);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-31 17:58:31
