/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.core.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author yesferal
 */
public class Producto implements Serializable{
    private Integer producto_id;
    private Integer categoria_id;
    private String producto_nombre;
    private String producto_descripcion;
    private Integer producto_stock;
    private Boolean producto_perecible;
    private Date producto_fVencimiento;
    private Float producto_precio;
    private Boolean producto_estado;
    private Categoria categoria;
    
    
    public Producto() {
        categoria = new Categoria();
    }

    public Integer getProducto_id() {
        return producto_id;
    }

    public void setProducto_id(Integer producto_id) {
        this.producto_id = producto_id;
    }

    public Integer getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(Integer categoria_id) {
        this.categoria_id = categoria_id;
    }

    public String getProducto_nombre() {
        return producto_nombre;
    }

    public void setProducto_nombre(String producto_nombre) {
        this.producto_nombre = producto_nombre;
    }

    public String getProducto_descripcion() {
        return producto_descripcion;
    }

    public void setProducto_descripcion(String producto_descripcion) {
        this.producto_descripcion = producto_descripcion;
    }

    public Integer getProducto_stock() {
        return producto_stock;
    }

    public void setProducto_stock(Integer producto_stock) {
        this.producto_stock = producto_stock;
    }

    public Boolean getProducto_perecible() {
        return producto_perecible;
    }

    public void setProducto_perecible(Boolean producto_perecible) {
        this.producto_perecible = producto_perecible;
    }

    public Date getProducto_fVencimiento() {
        return producto_fVencimiento;
    }

    public void setProducto_fVencimiento(Date producto_fVencimiento) {
        this.producto_fVencimiento = producto_fVencimiento;
    }

    public Float getProducto_precio() {
        return producto_precio;
    }

    public void setProducto_precio(Float producto_precio) {
        this.producto_precio = producto_precio;
    }

    public Boolean getProducto_estado() {
        return producto_estado;
    }

    public void setProducto_estado(Boolean producto_estado) {
        this.producto_estado = producto_estado;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }   
    
}
