/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.core.entity;

import java.io.Serializable;

/**
 * @author yesferal
 */
public class Usuario implements Serializable{
    private Integer usuario_id;
    private String usuario_nombre;
    private String usuario_apellido;
    private String usuario_correo;
    private String usuario_clave;
    private Integer usuario_rol;

    public Integer getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(Integer usuario_id) {
        this.usuario_id = usuario_id;
    }

    public String getUsuario_nombre() {
        return usuario_nombre;
    }

    public void setUsuario_nombre(String usuario_nombre) {
        this.usuario_nombre = usuario_nombre;
    }

    public String getUsuario_apellido() {
        return usuario_apellido;
    }

    public void setUsuario_apellido(String usuario_apellido) {
        this.usuario_apellido = usuario_apellido;
    }

    public String getUsuario_correo() {
        return usuario_correo;
    }

    public void setUsuario_correo(String usuario_correo) {
        this.usuario_correo = usuario_correo;
    }

    public String getUsuario_clave() {
        return usuario_clave;
    }

    public void setUsuario_clave(String usuario_clave) {
        this.usuario_clave = usuario_clave;
    }

    public Integer getUsuario_rol() {
        return usuario_rol;
    }

    public void setUsuario_rol(Integer usuario_rol) {
        this.usuario_rol = usuario_rol;
    }

    
}
