/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.core.entity;

import java.io.Serializable;

/**
 *
 * @author yesferal
 */
public class Categoria implements Serializable{
    private Integer categoria_id;
    private String categoria_nombre;
    private Boolean categoria_estado;

    public Categoria() {

    }
     
    public int getCategoria_id() {
        return categoria_id;
    }

    public void setCategoria_id(Integer categoria_id) {
        this.categoria_id = categoria_id;
    }

    public String getCategoria_nombre() {
        return categoria_nombre;
    }

    public void setCategoria_nombre(String categoria_nombre) {
        this.categoria_nombre = categoria_nombre;
    }

    public Boolean getCategoria_estado() {
        return categoria_estado;
    }

    public void setCategoria_estado(Boolean categoria_estado) {
        this.categoria_estado = categoria_estado;
    }
    
    
}
