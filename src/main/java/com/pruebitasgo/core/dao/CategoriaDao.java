/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.core.dao;

import com.pruebitasgo.core.entity.Categoria;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author yesferal
 */
public class CategoriaDao extends Conexion implements IBaseDao<Categoria, Integer>{

    @Override
    public void insertar(Categoria entity) throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        String sql = "INSERT INTO categoria(categoria_nombre,categoria_estado)VALUES(?,?)";
        int i = 1;
        obtenerConexion();
        super.preparedStatement = super.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        super.preparedStatement.setString(i, entity.getCategoria_nombre().trim().toUpperCase()); i++;
        super.preparedStatement.setBoolean(i, true);
        super.preparedStatement.executeUpdate();
        super.resultSet = super.preparedStatement.getGeneratedKeys();
        while(super.resultSet.next()){
                entity.setCategoria_id(super.resultSet.getInt(1));
        }
        liberarRecursos();
    }

    @Override
    public void actualizar(Categoria entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(Categoria entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Categoria obtener(Integer id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Categoria> listar(String filtro) throws Exception {
        Categoria categoria = null;
        List<Categoria> lista = new  ArrayList<Categoria>();
        String sql = "SELECT categoria_id, categoria_nombre, categoria_estado FROM categoria WHERE categoria_estado = 1 ORDER BY categoria_nombre";
        int i = 1;
        obtenerConexion();
        super.preparedStatement = super.connection.prepareStatement(sql);
        //super.preparedStatement.setString(i, filtro.trim().toUpperCase());
        super.resultSet = super.preparedStatement.executeQuery();
        while(super.resultSet.next()){
                i = 1;
                categoria = new Categoria();
                categoria.setCategoria_id(super.resultSet.getInt(i)); i++;
                categoria.setCategoria_nombre(super.resultSet.getString(i)); i++;
                categoria.setCategoria_estado(super.resultSet.getBoolean(i));
                lista.add(categoria);
        }
        liberarRecursos();
        return lista;
    }
    
}
