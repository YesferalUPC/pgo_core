/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.core.dao;
import com.pruebitasgo.core.entity.Producto;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author yesferal
 */
public class ProductoDao  extends Conexion implements IBaseDao<Producto, Integer>{

    @Override
    public void insertar(Producto entity) throws Exception {
        String sql = "INSERT INTO producto(categoria_id, producto_nombre, producto_descripcion,\n"+
                "producto_stock, producto_perecible, producto_fVencimiento,\n"+
                "producto_precio, producto_estado)VALUES(?,?,?,?,?,?,?,?)";
        int i = 1;
        obtenerConexion();
        super.preparedStatement = super.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        super.preparedStatement.setInt(i, entity.getCategoria_id()); i++;
        super.preparedStatement.setString(i, entity.getProducto_nombre().trim().toUpperCase()); i++;
        super.preparedStatement.setString(i, entity.getProducto_descripcion().trim().toUpperCase()); i++;
        super.preparedStatement.setInt(i, entity.getProducto_stock()); i++;
        super.preparedStatement.setBoolean(i, entity.getProducto_perecible()); i++;
        super.preparedStatement.setDate(i, new java.sql.Date(entity.getProducto_fVencimiento().getTime()));i++;
        super.preparedStatement.setFloat(i, entity.getProducto_precio());i++;
        super.preparedStatement.setBoolean(i, true);
        super.preparedStatement.executeUpdate();
        super.resultSet = super.preparedStatement.getGeneratedKeys();
        while(super.resultSet.next()){
                entity.setProducto_id(super.resultSet.getInt(1));
        }
        liberarRecursos();
    }

    @Override
    public void actualizar(Producto entity) throws Exception {
        String sql = "UPDATE producto SET categoria_id=?, producto_nombre=?, producto_descripcion=?,\n"+
                "producto_stock=?, producto_perecible=?, producto_fVencimiento=?,\n"+
                "producto_precio=? WHERE producto_id = ?";
        int i = 1;
        obtenerConexion();
        super.preparedStatement = super.connection.prepareStatement(sql);
        super.preparedStatement.setInt(i, entity.getCategoria_id()); i++;
        super.preparedStatement.setString(i, entity.getProducto_nombre().trim().toUpperCase()); i++;
        super.preparedStatement.setString(i, entity.getProducto_descripcion().trim().toUpperCase()); i++;
        super.preparedStatement.setInt(i, entity.getProducto_stock()); i++;
        super.preparedStatement.setBoolean(i, entity.getProducto_perecible()); i++;
        super.preparedStatement.setDate(i, new java.sql.Date(entity.getProducto_fVencimiento().getTime()));i++;
        super.preparedStatement.setFloat(i, entity.getProducto_precio());i++;
        
        super.preparedStatement.setInt(i, entity.getProducto_id());
        super.preparedStatement.executeUpdate();
        liberarRecursos();    
    }

    @Override
    public void eliminar(Producto entity) throws Exception {
        String sql ="UPDATE producto SET producto_estado = 0 WHERE producto_id = ?";
        int i = 1;
        obtenerConexion();
        super.preparedStatement = super.connection.prepareStatement(sql);
        super.preparedStatement.setInt(i, entity.getProducto_id());
        super.preparedStatement.executeUpdate();
        liberarRecursos();
    }

    @Override
    public Producto obtener(Integer id) throws Exception {
        Producto producto = null;
		String sql = "SELECT producto_id, categoria_id, producto_nombre, producto_descripcion,\n"+
                "producto_stock, producto_perecible, producto_fVencimiento,\n"+
                "producto_precio, producto_estado FROM producto WHERE producto_id = ?";
		int i = 1;
		obtenerConexion();
		super.preparedStatement = super.connection.prepareStatement(sql);
		super.preparedStatement.setInt(1, id);
		super.resultSet  = super.preparedStatement.executeQuery();
		while(super.resultSet.next()){
			producto = new Producto();
                        producto.setProducto_id(super.resultSet.getInt(i)); i++;
                        producto.setProducto_nombre(super.resultSet.getString(i));i++;
                        producto.setProducto_descripcion(super.resultSet.getString(i));i++;
                        producto.setProducto_stock(super.resultSet.getInt(i));i++;
                        producto.setProducto_perecible(super.resultSet.getBoolean(i));i++;
                        producto.setProducto_fVencimiento(super.resultSet.getDate(i));i++;
                        producto.setProducto_precio(super.resultSet.getFloat(i));i++;
                        producto.setProducto_estado(super.resultSet.getBoolean(i));i++;
		}
		liberarRecursos();
		return producto;
    }

    @Override
    public List<Producto> listar(String filtro) throws Exception {
        String sql = "SELECT producto_id, producto_nombre, producto_descripcion,\n"+
                "producto_stock, producto_perecible, producto_fVencimiento,\n"+
                "producto_precio, producto_estado, categoria_id FROM producto WHERE producto_estado = 1 AND producto_nombre like concat(?,'%') ORDER BY producto_nombre";
        Producto producto = null;
        List<Producto> lista = new ArrayList<Producto>();
        int i = 1;
        obtenerConexion();
        super.preparedStatement = super.connection.prepareStatement(sql);
        super.preparedStatement.setString(i, filtro.trim().toUpperCase());
        super.resultSet  = super.preparedStatement.executeQuery();
        while(super.resultSet.next()){
                i = 1;
                producto = new Producto();
                producto.setProducto_id(super.resultSet.getInt(i)); i++;
                producto.setProducto_nombre(super.resultSet.getString(i));i++;
                producto.setProducto_descripcion(super.resultSet.getString(i));i++;
                producto.setProducto_stock(super.resultSet.getInt(i));i++;
                producto.setProducto_perecible(super.resultSet.getBoolean(i));i++;
                producto.setProducto_fVencimiento(super.resultSet.getDate(i));i++;
                producto.setProducto_precio(super.resultSet.getFloat(i));i++;
                producto.setProducto_estado(super.resultSet.getBoolean(i));i++;
                producto.setCategoria_id(super.resultSet.getInt(i));
                lista.add(producto);
        }
        liberarRecursos();
        return lista;
    }
    
}
