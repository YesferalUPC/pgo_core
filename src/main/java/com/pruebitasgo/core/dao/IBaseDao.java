/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.core.dao;

import java.util.List;

/**
 *
 * @author yesferal
 */
public interface IBaseDao<E, J> {
	
	void insertar(E entity) throws Exception;
	
	void actualizar(E entity) throws Exception;
	
	void eliminar(E entity) throws Exception;
	
	E obtener(J id) throws Exception;
	
	List<E> listar(String filtro) throws Exception;
	
}
