/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.core.dao;

import com.pruebitasgo.core.entity.Usuario;
import java.util.List;

/**
 *
 * @author yesferal
 */
public class UsuarioDao extends Conexion implements IBaseDao<Usuario, Integer>{

    @Override
    public void insertar(Usuario entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(Usuario entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(Usuario entity) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Usuario obtener(Integer id) throws Exception {
        Usuario u = null;
        String sql = "SELECT usuario_id, usuario_nombre, usuario_apellido, usuario_correo, usuario_clave, usuario_rol FROM usuario WHERE usuario_id=? ORDER BY usuario_apellido";
        int i = 1;
        obtenerConexion();
        super.preparedStatement = super.connection.prepareStatement(sql);
        super.preparedStatement.setInt(i, id);
        super.resultSet = super.preparedStatement.executeQuery();
        while(super.resultSet.next()){
                u = new Usuario();
                u.setUsuario_id(super.resultSet.getInt(i)); i++;
                u.setUsuario_nombre(super.resultSet.getString(i)); i++;
                u.setUsuario_apellido(super.resultSet.getString(i)); i++;
                u.setUsuario_correo(super.resultSet.getString(i)); i++;
                u.setUsuario_clave(super.resultSet.getString(i)); i++;
                u.setUsuario_rol(super.resultSet.getInt(i)); 
        }
        liberarRecursos();
        return u;
    }

    public Usuario obtener(String correo, String clave) throws Exception {
		Usuario u = null;
		String sql = "SELECT usuario_id, usuario_nombre, usuario_apellido, usuario_correo, usuario_clave, usuario_rol FROM usuario WHERE usuario_correo = ? and usuario_clave = ? ORDER BY usuario_apellido";
		int i = 1;
		obtenerConexion();
		super.preparedStatement = super.connection.prepareStatement(sql);
		super.preparedStatement.setString(i, correo); i++;
		super.preparedStatement.setString(i, clave);
		super.resultSet = super.preparedStatement.executeQuery();
		while(super.resultSet.next()){
                    i = 1;
                    u = new Usuario();
                    u.setUsuario_id(super.resultSet.getInt(i)); i++;
                    u.setUsuario_nombre(super.resultSet.getString(i)); i++;
                    u.setUsuario_apellido(super.resultSet.getString(i)); i++;
                    u.setUsuario_correo(super.resultSet.getString(i)); i++;
                    u.setUsuario_clave(super.resultSet.getString(i)); i++;
                    u.setUsuario_rol(super.resultSet.getInt(i));			
		}
		liberarRecursos();
		return u;
	}
    
    @Override
    public List<Usuario> listar(String filtro) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
