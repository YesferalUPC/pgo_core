/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.core.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * @author yesferal
 */

public final class UtilCore {

	private final static String ARCHIVO = "propiedades";

    private UtilCore() {
    }

    //Metodo para leer un archivo de properties
    public static final String obtenerPropiedad(String clave) {
        ResourceBundle rs = ResourceBundle.getBundle(ARCHIVO);
        return rs.getString(clave);
    }

    //Dar formato a una fecha
    public static final String convertirDate(Date fecha, String formato) {
        SimpleDateFormat sdf = new SimpleDateFormat(formato);
        return sdf.format(fecha);
    }
}
