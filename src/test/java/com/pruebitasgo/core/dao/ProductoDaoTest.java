/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebitasgo.core.dao;

import com.pruebitasgo.core.entity.Producto;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author yesferal
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductoDaoTest {
    
    private static Producto p;
    private static final ProductoDao productoDao = new ProductoDao();
    
    public ProductoDaoTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of insertar method, of class ProductoDao.
     */
    @Test
    public void a_testInsertar() throws Exception {
        System.out.println("****************************************************");        
        System.out.println("* * *    INICIO J.UNIT: PRUEBAS UNITARIAS      * * *");
        System.out.println("****************************************************");
        System.out.println("*   Ejecutando prueba de insercion . . .           *");
        try{
            p = new Producto();
            Calendar calendario = GregorianCalendar.getInstance();
            Date fecha = calendario.getTime();
            p.setCategoria_id(1);
            p.setProducto_nombre("Cafe");
            p.setProducto_descripcion("Producto peruano, producido por la empresa El Cafecito");
            p.setProducto_stock(1);
            p.setProducto_precio(10.10f);
            p.setProducto_fVencimiento(fecha);
            p.setProducto_perecible(false);
            p.setProducto_estado(true);
            
            productoDao.insertar(p);
            assertTrue(p.getProducto_id() > 0);
        System.out.println("*   exito al ejecutar la prueba de insercion.      *");
        }catch(Exception e){
            e.printStackTrace();
            fail("Ocurrio un error "+e.getMessage());
        }
    }
    
    @Test
    public void a_testInsertar_Alternativo() throws Exception {
        System.out.println("*   Ejecutando prueba 2 de insercion . . .         *");
        try{
            p = new Producto();
            Calendar calendario = GregorianCalendar.getInstance();
            Date fecha = calendario.getTime();
            p.setCategoria_id(1);
            p.setProducto_nombre("Inka Cola");
            p.setProducto_descripcion("Gaseosa");
            p.setProducto_stock(1);
            p.setProducto_precio(1f);
            p.setProducto_fVencimiento(fecha);
            p.setProducto_perecible(false);
            p.setProducto_estado(true);
            
            productoDao.insertar(p);
            assertTrue(p.getProducto_id() > 0);
        System.out.println("*   exito al ejecutar la prueba 2 de insercion.    *");
        }catch(Exception e){
            e.printStackTrace();
            fail("Ocurrio un error "+e.getMessage());
        }
    }

    /**
     * Test of actualizar method, of class ProductoDao.
     */
    @Test
    public void c_testActualizar() throws Exception {
        System.out.println("*   Ejecutando prueba de actualizacion . . .       *");
        try{            
            Calendar calendario = GregorianCalendar.getInstance();
            Date fecha = calendario.getTime();
            
            p.setCategoria_id(2);
            p.setProducto_nombre("Chocolate");
            p.setProducto_descripcion("Producto peruano, producido por la empresa El Choco");
            p.setProducto_stock(1);
            p.setProducto_precio(10.10f);
            p.setProducto_fVencimiento(fecha);
            p.setProducto_perecible(true);
            p.setProducto_estado(false);
            
            productoDao.actualizar(p);
            assertTrue(p.getCategoria_id() == 2);
            assertTrue(p.getProducto_nombre().equals("Chocolate"));
            assertTrue(p.getProducto_descripcion().equals("Producto peruano, producido por la empresa El Choco"));
            assertTrue(p.getProducto_stock() == 1);
            assertTrue(p.getProducto_precio() == 10.10f);
            assertTrue(p.getProducto_perecible() == true);
            assertTrue(p.getProducto_estado() == false);
        System.out.println("*   exito al ejecutar la  prueba de actualizacion. *");
        }catch(Exception e){
            e.printStackTrace();
            fail("Ocurrio un error "+e.getMessage());
        }
    }

    /**
     * Test of eliminar method, of class ProductoDao.
     */
    @Test
    public void d_testEliminar() throws Exception {
        System.out.println("*   Ejecutando prueba de eliminacion . . .         *");
        try{
            productoDao.eliminar(p);
            assertTrue(true);
        System.out.println("*   exito al ejecutar la  prueba de eliminacion.   *");
        System.out.println("*                                                  *");
        System.out.println("****************************************************");
        }catch(Exception e){
            e.printStackTrace();
            fail("Ocurrio un error "+e.getMessage());
        }
    }

     /**
     * Test of listar method, of class ProductoDao.
     */
    @Test
    public void b_testListar() throws Exception {
        System.out.println("*   Ejecutando prueba para listar productos . . .  *");
        try{
            List<Producto> productos = productoDao.listar("");
            assertTrue(productos.size() > 0);
        System.out.println("*   exito al ejecutar la  prueba para listar.      *");

        }catch(Exception e){
            e.printStackTrace();
            fail("Ocurrio un error "+e.getMessage());
        }
    }
    
}
